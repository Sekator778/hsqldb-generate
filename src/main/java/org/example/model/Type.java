package org.example.model;

public enum Type {
    FOOD,
    DRINK,
    ELECTRONIC
}
